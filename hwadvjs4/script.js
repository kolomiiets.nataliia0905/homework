const BASE_URL = 'https://ajax.test-danit.com/api/swapi/films';
const root = document.querySelector('#root');

class Films {
    constructor(url) {
        this.url = url;
    }

    get() {
        return fetch(this.url).then((response) => {
            return response.json();
        })
    }

    show(root) {
        this.get().then((data) => {
            const films = data.map((el) => {
                const li = document.createElement('li');
                const filmId = document.createElement('p');
                const filmName = document.createElement('h1');
                const openingCrawl = document.createElement('p');

                el.characters.forEach((elem) => {
                    fetch(elem).then((response) => {
                        return response.json();
                    }).then((data)=>{
                        const person = document.createElement('p');
                        person.textContent = data.name;
                        filmName.after(person);
                    })
                })


                filmId.textContent = el.episodeId;
                filmName.textContent = el.name;
                openingCrawl.textContent = el.openingCrawl;

                li.append(filmId, filmName, openingCrawl);
                return li;
            })
            const list = document.createElement('ul');
            list.append(...films);
            root.append(list)
        })
    }
}

const films = new Films(BASE_URL);
films.show(root);





