const books = [

    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];
const root = document.getElementById("root");
const arrayWords = ["name", "author", "price"];

function createList(array, parent, arrayWords) {
    const list = document.createElement("ul");
    array.forEach(obj => {
        try {
            let result = "";
            let errorMessage = "";
            arrayWords.forEach(key => {
                if (obj[key]) {
                    result += ` ${key} : ${obj[key]}\n`
                    // result=result+` ${key} : ${obj[key]}\n`
                } else {
                    errorMessage += ` ${key} `
                }
            })

            if (errorMessage) {
                throw new Error(errorMessage)
            }
            let li = document.createElement("li");
            li.textContent = result;
            list.append(li);


        } catch (e) {
            console.log(JSON.stringify(obj));
            console.log(`Нету таких данных :`);

            console.log(e.message);
        }

    })

    parent.append(list);

}

createList(books, root, arrayWords);