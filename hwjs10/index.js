const buttons = document.querySelectorAll('.tabs-title');
buttons.forEach((btn)=>{
    btn.addEventListener('click', clickButton)
})
function clickButton (event){
    let buttonAtr = this.getAttribute('data-id');
    let text = document.querySelector(`.tabs-content [data-id = ${buttonAtr}]`);
    document.querySelector('.show').classList.remove('show');
    text.classList.add('show');
    document.querySelector('.tabs-title.active').classList.remove('active');
    this.classList.add('active');
}

