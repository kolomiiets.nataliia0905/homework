let timerId = setInterval(slider, 3000);
const imgs = document.querySelectorAll(".image-to-show");
let count = 1;

function slider () {
    imgs.forEach(img => {
        img.classList.remove("show");
    })
    if (imgs.length===count){
        count = 0;
    }

imgs[count].classList.add("show");
    count++
}

const stopBtn = document.createElement("button");
stopBtn.textContent = "Припинити";
document.body.append(stopBtn);
console.log();
stopBtn.addEventListener("click",() => {
    clearInterval(timerId);
})


const playBtn = document.createElement("button");
playBtn.textContent = "Відновити показ";
document.body.append(playBtn);
console.log();
playBtn.addEventListener("click",() => {
    timerId = setInterval(slider, 3000);
})
