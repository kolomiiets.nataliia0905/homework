const btn = document.createElement("button");
btn.textContent = "Змінити тему";
document.body.append(btn);
btn.style.position= "fixed";
btn.style.top= "10px";
btn.style.right= "10px";
function changeColorBody() {
    if (localStorage.getItem("bqColor")==="yellow") {
        document.body.classList.add("yellow");
    } else {
        document.body.classList.remove("yellow")
    }
}
changeColorBody()
btn.addEventListener("click",() => {
   const changeValue = localStorage.getItem("bqColor")==="yellow"?"":"yellow";
   localStorage.setItem("bqColor",changeValue)
    changeColorBody()
})