export default class Modal {
    constructor(content, title) {
        this.content = content;
        this.title = title;
    }
    delete() {
        const modal = document.querySelector(".modal");
        modal.remove();
    }
render () {
document.body.insertAdjacentHTML("afterend", `<div class="modal" tabindex="-1">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">${this.title}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        ${this.content}
      </div>
      
    </div>
  </div>
</div>`)
    const modal = document.querySelector(".modal");
const btn = modal.querySelector("button");
   btn.onclick = this.delete
}
}