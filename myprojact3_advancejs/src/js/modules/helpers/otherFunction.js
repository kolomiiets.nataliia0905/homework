export const checkToken =() => {
    const entranceBtn = document.getElementById("entranceBtn");
    const creatBtn= document.getElementById("creatBtn");
    const token = localStorage.getItem("token");

    if (token) {
        entranceBtn.style.display = "none";
        creatBtn.style.display = "block";

    }
    else {
        entranceBtn.style.display = "block";
        creatBtn.style.display = "none";
    }
}