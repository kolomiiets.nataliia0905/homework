import Form from "./classes/Form.js";
import Modal from "./classes/Modal.js";
import {Visit} from "./classes/Visit.js";

const create = document.getElementById("creatBtn");
create.addEventListener("click", () => {
    const form =new Form();
    const modal =new Modal(form.renderCreatForm(),"Создать");
    modal.render();
    const selectDoctor = document.getElementById("selectDoctor");
    selectDoctor.addEventListener("change", () => {
        const formContent = document.getElementById("form-content");
        formContent.textContent = "";
        console.log(selectDoctor.value);
     function renderInput () {
         switch (selectDoctor.value) {
             case "dentist" :
                 return new Visit().renderInputs()
             default : return ""
         }
     }

        formContent.insertAdjacentHTML("beforeend", renderInput())

    })
})