document.addEventListener("keydown", e=> {
    console.log(e);
    const buttons = document.querySelectorAll("button");
    buttons.forEach(btn => {
        console.log(btn.textContent);
        btn.style.color = "";
        const btnText = btn.textContent.toLowerCase();
        if(e.key === btnText){
            btn.style.color = "blue";
        }
    })
})