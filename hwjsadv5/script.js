const BASE_URL = 'https://ajax.test-danit.com/api/json/';
const USERS_URL = `${BASE_URL}users`;
const POSTS_URL = `${BASE_URL}posts`;


const template = document.querySelector('#posts-list').content;
const root = document.querySelector('#root');

class Card {
    constructor(template) {
        this.template = template;
        this.card = this.template.querySelector(".card").cloneNode(true);
    }

    render(title, body, name, username, email, id) {
        const titleElement = this.card.querySelector('.title');
        const textElement = this.card.querySelector('.text');
        const nameElement = this.card.querySelector('.name');
        const userNameElement = this.card.querySelector('.userName');
        const emailElement = this.card.querySelector('.email');
        const buttonElement = document.createElement('button');
        titleElement.textContent = title;
        textElement.textContent = body;
        nameElement.textContent = name;
        userNameElement.textContent = '@'+ username;
        emailElement.textContent = email;
        emailElement.href = 'mailto:'+email;
        buttonElement.textContent = 'DELETE';
        buttonElement.className = 'btn btn-success';
        buttonElement.addEventListener('click',(event)=>{
            event.preventDefault();
            requests.delete(POSTS_URL+ '/'+ id)
                .then((data)=>{
                    this.card.remove();
                })
                .catch((e)=>{
                    console.log(e);
                })
        })
        emailElement.after(buttonElement);
        return this.card;
    }
}

class Requests {
    constructor() {
    }
    get(url) {
        return axios.get(url)
    }
    delete(url){
        return axios.delete(url);
    }
}

const requests = new Requests();

Promise.allSettled([requests.get(USERS_URL),
    requests.get(POSTS_URL)])
    .then((data) => {
        const {value: {data: users}} = data [0];
        const {value: {data: posts}} = data [1];
        posts.forEach((post) => {
            const user = users.find((user) => user.id === post.userId);
            const card = new Card(template);
            root.append(card.render(post.title, post.body, user.name, user.username, user.email, post.id));
        })
    })
    .catch((e)=>{
        console.log(e);
    });





